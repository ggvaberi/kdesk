#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>
#include <jpeglib.h>
#include <openssl/pem.h>

char *base64encode (const void *b64_encode_this, int encode_this_many_bytes);
int ImageToJpeg(XImage* img, u_int8_t** jpegBuf);

void MouseClickLeft() {
    XEvent event;
    Display *dpy = XOpenDisplay (NULL);

    /* Fake the mouse button Press and Release events */
    XTestFakeButtonEvent (dpy, 1, True,  CurrentTime);
    usleep(100000);
    XTestFakeButtonEvent (dpy, 1, False, CurrentTime);
    XCloseDisplay (dpy);
}

void MouseLeftDown() {
    XEvent event;
    Display *dpy = XOpenDisplay (NULL);

    /* Fake the mouse button Press and Release events */
    XTestFakeButtonEvent (dpy, 1, True,  CurrentTime);
    XCloseDisplay (dpy);
}

void MouseLeftUp() {
    XEvent event;
    Display *dpy = XOpenDisplay (NULL);

    /* Fake the mouse button Press and Release events */
    //XTestFakeButtonEvent (dpy, 1, True,  CurrentTime);
    XTestFakeButtonEvent (dpy, 1, False, CurrentTime);
    XCloseDisplay (dpy);
}

void SetMousePosition(int x, int y) {
  Display *dpy = XOpenDisplay(NULL);
  int scr = XDefaultScreen(dpy);
  Window rwn = XRootWindow(dpy, scr);

  XWarpPointer(dpy, 0, rwn, 0, 0, 0, 0, x, y);
  XFlush(dpy);
  XCloseDisplay(dpy);
}

void ScreenResolution(int* w, int* h) {
  fprintf(stderr, "xcmd: ScreenResolution. \n");
  Display *dpy = XOpenDisplay(NULL);
  int scr = XDefaultScreen(dpy);

  *w = XDisplayWidth(dpy, scr);
  *h = XDisplayHeight(dpy, scr);

  XCloseDisplay(dpy);
}

XImage* ReadScreenshot();

char* imgBuffer = NULL;

char* GetScreenshot() {
    fprintf(stderr, "Starting GetScreenshot.\n");

    XImage* img = ReadScreenshot();

    fprintf(stderr, "GetScreenshot img %p.\n", img);

    if (img == NULL)
        return 0;

    u_int8_t* jpeg = NULL;

    int size = ImageToJpeg(img, &jpeg);

    fprintf(stderr, "GetScreenshot jpeg %p, size %d.\n", jpeg, size);

    XDestroyImage(img);

    if (size < 1) {
        return 0;
    }

    {
        /*FILE* f = fopen("/tmp/1.jpg", "wb");

        if (f != NULL) {
            fwrite(jpeg, 1, size, f);
            fclose(f);
        }

        exit(1);*/
    }

    char* b64 = base64encode(jpeg, size);

    fprintf(stderr, "GetScreenshot tobase64 %p.\n", b64);

    free(jpeg);

    return b64;
}

XImage* ReadScreenshot() {
    fprintf(stderr, "ReadScreenshot\n");
    Display *display = NULL;
    int screen;
    Window root;
    display = XOpenDisplay(0);
    fprintf(stderr, "ReadScreenshot dpy %p\n", display);

    if (!dpy)
      return NULL;

    screen = DefaultScreen(display);
    fprintf(stderr, "ReadScreenshot screen  %i\n", screen);
    root = RootWindow(display, screen);
    XWindowAttributes gwa;

    XGetWindowAttributes(display, root, &gwa);
    int width = gwa.width;
    int height = gwa.height;
    fprintf(stderr, "Window resolution: w/h %d/%d\n", width, height);
    XImage *img = XGetImage(display, root, 0, 0, width, height, XAllPlanes(), ZPixmap);
    //fprintf(stderr, "Res: w/h %d/%d\n", width, height);
    XCloseDisplay(display);

    return img;
}

typedef struct { u_int8_t r, g, b; } Pixel;

int ImageToJpeg(XImage* img, u_int8_t** jpegBuf)
{
    if (img == NULL)
        return 0;

    if (img->width == 0 || img->height == 0)
        return 0;

    //u_int8_t *image = (u_int8_t*) malloc(3 * img->width * img->height);
    Pixel *image = (Pixel*) malloc(sizeof(Pixel) * img->width * img->height);

    {
        unsigned long red_mask;
        unsigned long green_mask;
        unsigned long blue_mask;

        red_mask   = img->red_mask;
        green_mask = img->green_mask;
        blue_mask  = img->blue_mask;

        for (int i = 0; i < (img->height - 1); i++) {
            for (int j = 0; j < (img->width - 1); j++) {
                unsigned long pixel = XGetPixel(img, j, i);

                // TODO(richard-to): Figure out why red and blue are swapped

                image[i * img->width + j].b = pixel & blue_mask;
                image[i * img->width + j].g = (pixel & green_mask) >> 8;
                image[i * img->width + j].r = (pixel & red_mask) >> 16;
            }
        }
        /*
        FILE* f= fopen("/tmp/222.data", "wb");
        if (f) {
            fwrite(image, 1, sizeof(Pixel) * img->width * img->height, f);
            fclose(f);
        }
        fprintf(stderr, "raw data 222 printed.\n");
        */
    }

    int quality = 80;
    const char *comment = "JPEG FROM KLIB";

    unsigned long jpegSize = 0;

    struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];
	int row_stride;

	cinfo.err = jpeg_std_error(&jerr);

	jpeg_create_compress(&cinfo);

	cinfo.image_width = img->width;
	cinfo.image_height = img->height;

	// Input is RGB, 3 byte per pixel
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;

	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, quality, TRUE);

	//
	//
	// Tell libJpeg to encode to memory, this is the bit that's different!
	// Lib will alloc buffer.
	//

	jpeg_mem_dest(&cinfo, jpegBuf, &jpegSize);

	jpeg_start_compress(&cinfo, TRUE);

	// Add comment section if any..
	if (comment) {
		jpeg_write_marker(&cinfo, JPEG_COM, (const JOCTET*)comment, strlen(comment));
	}

	// 1 BPP
	row_stride = img->width;

	// Encode
	while (cinfo.next_scanline < cinfo.image_height)
    {
		row_pointer[0] = &((u_int8_t*)image)[3 * cinfo.next_scanline * row_stride];
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);

    free(image);

    return jpegSize;
}

char *base64encode (const void *b64_encode_this, int encode_this_many_bytes)
{
    BIO *b64_bio, *mem_bio;      //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    BUF_MEM *mem_bio_mem_ptr;    //Pointer to a "memory BIO" structure holding our base64 data.
    b64_bio = BIO_new(BIO_f_base64());                      //Initialize our base64 filter BIO.
    mem_bio = BIO_new(BIO_s_mem());                           //Initialize our memory sink BIO.
    BIO_push(b64_bio, mem_bio);            //Link the BIOs by creating a filter-sink BIO chain.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);  //No newlines every 64 characters or less.
    BIO_write(b64_bio, b64_encode_this, encode_this_many_bytes); //Records base64 encoded data.
    BIO_flush(b64_bio);   //Flush data.  Necessary for b64 encoding, because of pad characters.
    BIO_get_mem_ptr(mem_bio, &mem_bio_mem_ptr);  //Store address of mem_bio's memory structure.
    BIO_set_close(mem_bio, BIO_NOCLOSE);   //Permit access to mem_ptr after BIOs are destroyed.
    BIO_free_all(b64_bio);  //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    BUF_MEM_grow(mem_bio_mem_ptr, (*mem_bio_mem_ptr).length + 1);   //Makes space for end null.
    (*mem_bio_mem_ptr).data[(*mem_bio_mem_ptr).length] = '\0';  //Adds null-terminator to tail.
    return (*mem_bio_mem_ptr).data; //Returns base-64 encoded data. (See: "buf_mem_st" struct).
}

/*
void MouseClickLeft() {
  fprintf(stderr, "Sending the event!\n");
    Display *display = XOpenDisplay(NULL);

    // Create and setting up the event
    XEvent event;
    memset(&event, 0, sizeof(event));
    event.xbutton.button = Button1Mask;
    event.xbutton.same_screen = True;
    event.xbutton.subwindow = DefaultRootWindow(display);

    while (event.xbutton.subwindow) {
        event.xbutton.window = event.xbutton.subwindow;
        XQueryPointer(display, event.xbutton.window,
                      &event.xbutton.root, &event.xbutton.subwindow,
                      &event.xbutton.x_root, &event.xbutton.y_root,
                      &event.xbutton.x, &event.xbutton.y,
                      &event.xbutton.state);
    }

    // Press
    event.type = ButtonPress;
    if (XSendEvent(display, PointerWindow, True, ButtonPressMask, &event) == 0)
        fprintf(stderr, "Error to send the event!\n");
    XFlush(display);

    // Release
    event.type = ButtonRelease;
    if (XSendEvent(display, PointerWindow, True, ButtonReleaseMask, &event) == 0)
        fprintf(stderr, "Error to send the event!\n");
    XFlush(display);

    XCloseDisplay(display);
}
*/
