
ifeq ($(OS),Windows_NT)
	cOS := Windows
else
	cOS := $(shell uname)
endif

ifeq ($(cOS),Windows)
	CC := gcc
	LDF := -ljpeg
	XCMD := win32/xcmd.c
endif

ifeq ($(cOS),Linux)
	CC := gcc
	CF := --shared -fPIC -g
	LF := -lXtst -ljpeg -lssl -lcrypto
	XCMD := linux/xcmd.c
endif

ifeq ($(cOS),Darwin)
	CC := clang
	CF := -dynamiclib -fPIC -g
	LF := -framework AppKit -framework Foundation -framework Cocoa
	XCMD := darwin/xcmd.mm
endif

all:
	$(CC) $(CF) -o xcmd.so $(XCMD) $(LF)
	go tool cgo main.go
	go build

clean:
	go clean
	rm -f xcmd.so kdesk
