var canvasX = -1;
var canvasY = -1;

var msGrub = false;

//$("#myimage").attr("src","data:image/jpg;base64," + "Qk02EA4AAA....")

function bodyLoad() {
    startAuth();

    /*var viewFullScreen = document.getElementById("view-fullscreen");
    if (viewFullScreen) {
        viewFullScreen.addEventListener("click", function() {
            toggleFullScreen();
        })
    }*/
}

function windowResize(e) {
    var screen = document.getElementById('screen');

    screen.width = document.body.clientWidth;
    screen.height = document.body.clientHeight;
}

function screenMouseMove(e) {
    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    let x = e.offsetX;
    let y = e.offsetY;

    let w = document.body.clientWidth;
    let h = document.body.clientHeight;

    ajax.open('GET', "/mousemove?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
    ajax.send();

    drawTrace(x, y);
}

function startAuth() {
    console.log('log: start authentication.');

    var $dlg = $("<div id='dialog' title='Basic dialog'\> \
    <input id='uname' type='text' value='u'>user</input> \
    <input id='upass' type='password' value='u'>pass</input> \
    <input id='ulogin' type='button' value='login'></input> \
    </div>");

    $("body").append($dlg);
    $dlg.hide();
    $( "#dialog" ).dialog();

    $("#ulogin").click(function(){
        var user = $("#uname").val();
        var pass = $("#upass").val();
        user = "u";
        pass = "u";
        $( "#dialog" ).dialog( "destroy" );
        $( "#dialog" ).remove();
        doAuth(user, pass);
    });

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    ajax.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //Typical action to be performed when the document is ready:
            //document.getElementById("demo").innerHTML = xhttp.responseText;
            console.log('Auth attempt: ' + this.responseText);

            if (this.responseText == 'YES') {

                // Update session
                setInterval(function(){
                    var ajax = new XMLHttpRequest();

                    if (ajax == null) {
                        return;
                    }

                    ajax.open('GET', "/auth");
                    ajax.send();
                }, 9 * 60000);
            }
        }
    };

    ajax.open('GET', "/auth");
    ajax.send();

}

function doAuth(user, pass) {
    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    ajax.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //Typical action to be performed when the document is ready:
            //document.getElementById("demo").innerHTML = xhttp.responseText;
            console.log('Auth attempt: ' + this.responseText);

            if (this.responseText == 'YES') {
                //var $menu = $("<ol id='startMenu'> \
                //        <li id='m_audio' class='ui-widget-content'>Audio control</li> \
                //        <li id='m_point' class='ui-widget-content'>Point catch</li> \
                //    </ol>");
                var $content=$("<ol id='selectable'>");
                $content.append("<li class='ui-widget-content'>Audio</li>");
                $content.append("<li class='ui-widget-content'>Paint</li>");
                $content.append("</ol>");
                $("body").append($content);

                $('.ui-widget-content').on("click", function () {
                    var tmp = $(this).text()
                    $('#selectable').remove();

                    if (tmp == 'Audio') {
                        showAudio();
                    } else if (tmp == 'Paint') {
                        showPaint();
                    }
                });
            } else {
                alert("failed");
            }
        }
    }

    ajax.open('POST', "/auth?" + "user=" + user + "&" + "pass=" + pass);
    ajax.send();
}

function showAudio() {
    var $dialog = $("<div id='dialog' title='Audio volume'\> \
                        <div id='slider'></div> \
                    </div>");
    $( "body" ).append($dialog);
    $( "#dialog" ).dialog();
    $( "#slider" ).slider().bind({
        change: function() {
        },
    });
    var ajax = new XMLHttpRequest();

    if (ajax) {
        ajax.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //Typical action to be performed when the document is ready:
                //document.getElementById("demo").innerHTML = xhttp.responseText;
                //console.log('Auth attempt: ' + this.responseText);
                $("#slider").slider('value', this.responseText);
            }
        }

        ajax.open('GET', "/getavolume");
        ajax.send();
    }

    $( "#slider" ).on( "slidechange", function( event, ui ) {
        var v = ui.value;

        var ajax = new XMLHttpRequest();

        if (ajax) {
            ajax.open('GET', "/setavolume?v=" + v);
            ajax.send();
        }
    });
}

function showPaint() {
    var screen = document.getElementById('screen');
    var context = screen.getContext('2d');

    screen.width = document.body.clientWidth;
    screen.height = document.body.clientHeight;

    screen.addEventListener('mousemove', screenMouseMove);

    //screen.addEventListener('dblclick', screenMouseClick);
    screen.addEventListener('mouseup',  screenMouseUp);
    screen.addEventListener('mousedown',  screenMouseDown);

    screen.addEventListener('touchmove',  screenTouchMove);
    screen.addEventListener('touchstart', screenTouchStart);
    screen.addEventListener('touchend',   screenTouchEnd);

    window.addEventListener("resize", windowResize);

    startScreenshots();
}

function goFullScreen(){

    var element = document.body;

    // Check which implementation is available
    var requestMethod = element.requestFullScreen ||
              element.webkitRequestFullscreen ||
              element.webkitRequestFullScreen ||
              element.mozRequestFullScreen ||
              element.msRequestFullscreen;

    if( requestMethod ) {
      requestMethod.apply( element );
      //requestMethod();
      //element.requestFullScreen();
    }
}

function toggleFullScreen() {
    if (!document.fullscreenElement) {
        goFullScreen();
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      }
    }
}

function screenMouseClick(e) {
    console.log('log: screen mouse click.');

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var x = e.offsetX;
    var y = e.offsetY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    ajax.open('GET', "/mouseclick?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
    ajax.send();

    drawTrace(x, y);
}

function screenMouseUp(e) {
    console.log('log: screen mouse up.');

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var x = e.offsetX;
    var y = e.offsetY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    ajax.open('GET', "/mouseup?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
    ajax.send();

    drawTrace(x, y);
}

function screenMouseDown(e) {
    console.log('log: screen mouse down.');

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var x = e.offsetX;
    var y = e.offsetY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    ajax.open('GET', "/mousedown?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
    ajax.send();

    drawTrace(x, y);
}

var startDate = new Date();
var moveDate = new Date();

function screenTouchMove(e) {
    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var f = 0.0;

    for (var i=0; i < e.targetTouches.length; i++) {
        console.log("targetTouches[" + i + "].force = " + e.targetTouches[i].force);
        f += e.targetTouches[i].force;
    }

    if (f < 0.1) {
    }

    var x = e.touches[0].clientX;
    var y = e.touches[0].clientY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    if (!msGrub) {
        var date = new Date();

        if ((date.getTime() - startDate.getTime()) > 100) {
            ajax.open('GET', "/mousedown?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h + "&f=" + f);
            ajax.send();
            msGrub = true;
            return
        }
    }

    ajax.open('GET', "/mousemove?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h + "&f=" + f);
    ajax.send();

    moveDate = new Date();

    drawTrace(x, y);
}

function screenTouchStart(e) {
    var date = new Date();

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var x = e.touches[0].clientX;
    var y = e.touches[0].clientY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    msGrub = false;

    if ((date.getTime() - startDate.getTime()) < 200) {
        ajax.open('GET', "/mouseclick?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
        ajax.send();
    } else {
        msGrub = false;
        ajax.open('GET', "/mouseup?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
        ajax.send();
    }

    startDate = date;

    drawTrace(x, y);
}

function screenTouchEnd(e) {
    var endDate = new Date();

    var del = endDate.getTime() - startDate.getTime();

    if (del < 500) {
    }

    msGrub = false;

    var ajax = new XMLHttpRequest();

    if (ajax == null) {
        return;
    }

    var x = e.touches[0].clientX;
    var y = e.touches[0].clientY;

    var w = document.body.clientWidth;
    var h = document.body.clientHeight;

    ajax.open('GET', "/mouseup?x=" + x + "&y=" + y + "&w=" + w + "&h=" + h);
    ajax.send();

    drawTrace(x, y);
}

function drawTrace(x, y) {
    var screen = document.getElementById('screen');
    var context = screen.getContext('2d');

    if (canvasX != -1) {
        context.clearRect(canvasX - 15, canvasY - 15, 30, 30);
    }

    context.beginPath();
    context.arc(x, y, 10, 0, 2 * Math.PI, false);
    context.fillStyle = 'green';
    context.fill();
    context.lineWidth = 5;
    context.strokeStyle = '#003300';
    context.stroke();

    canvasX = x;
    canvasY = y;
}

function startScreenshots() {
    // Request screenshots
                setInterval(function(){
                    var ajax = new XMLHttpRequest();

                    if (ajax == null) {
                        return;
                    }

                    ajax.open('GET', "/screenshot?x=0");
                    ajax.send();
                    ajax.onload = function(e) {
                        //console.log("Got screenshot " + ajax.responseText);

                        var img = document.getElementById("screenshot");

                        if (img == null)
                            return;

                        img.setAttribute("src", "data:image/jpg;base64,"  + ajax.responseText);

                        var screen = document.getElementById('screen');
                        var ctx = screen.getContext('2d');

                        var height = document.body.clientHeight;
                        var width = document.body.clientWidth;

                        ctx.drawImage(img, 0, 0, width, height);
                    }
                }, 100);
}