package main

/*
#cgo LDFLAGS:  -ldl
#include <dlfcn.h>
#include <stdlib.h>
#include <errno.h>

typedef void (*ptr_mouse_click_left)(int x, int y);
typedef void (*ptr_mouse_left_down)();
typedef void (*ptr_mouse_left_up)();
typedef void (*ptr_screen_resolution)(int* w, int* h);
typedef void (*ptr_set_mouse_position)(int x, int y);
typedef char* (*ptr_get_screenshot)();

int mouse_click_left(void* f, int x, int y) { // wrapper function
    ((ptr_mouse_click_left) f)(x, y);

    return 0;
}

int mouse_left_down(void* f) {
    ((ptr_mouse_left_down) f)();

    return 0;
}

int mouse_left_up(void* f) {
    ((ptr_mouse_left_up) f)();

    return 0;
}

int screen_resolution(void* f, int* w, int* h) {
    ((ptr_screen_resolution) f)(w, h);

    return 0;
}

int set_mouse_position(void* f, int x, int y) {
    ((ptr_set_mouse_position) f)(x, y);

    return 0;
}

char* get_screenshot(void* f) {
    return ((ptr_get_screenshot) f)();
}
*/
import "C"

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
	"unsafe"

	"github.com/google/uuid"
)

var ptGrub bool = false

var startDate time.Time
var clientIP = ""
var sessionID = ""

var uname string = ""
var upass string = ""

// read file in string
func readFile(path string) string {
	f, err := os.Open(path)

	if err != nil {
		log.Fatalf("unable to open file: %v", err)

		return ""
	}

	defer f.Close()

	b := make([]byte, 0)

	for {
		t := make([]byte, 4096)

		n, err := f.Read(t)

		log.Println("read file data len: %v", n)

		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatalf("unable to read file: %v", err)

			break
		}

		b = append(b, t[:n]...)
	}

	log.Println("Successfully red file: ", path, " len: ", len(b))

	return string(b)
}

func sessionValid(r *http.Request) bool {
	sid := ""

	for _, c := range r.Cookies() {
		//fmt.Println(c.Name, "", c.Value)
		if c.Name == "SID" {
			sid = c.Value
			break
		}
	}

	if sessionID == "" {
		//log.Println("Invalid session id")

		return false
	} else {
		if sid != sessionID {
			log.Println("Client session log not equal to current session id: ", sessionID, " ", sid)

			return false
		}
	}

	return true
}

// FnRoute is ...
type FnRoute func(http.ResponseWriter, *http.Request)

// WebRoute is ...
type WebRoute struct {
	id     string
	handle FnRoute
}

// WebHandler is ...
type WebHandler struct {
	mu sync.Mutex // guards n
	n  int

	routes []*WebRoute
}

func (h *WebHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//log.Println("Run web handler " + r.URL.Path)

	var url = r.URL.Path

	//if len(url) > 7 {
	//	log.Println("Run web  " + r.URL.Path[0:8])
	//}

	if len(url) > len("/static/") && url[0:8] == "/static/" {
		log.Println("Handle static")
		handleStatic(w, r)
	} else {
		//log.Println("routes count " + strconv.Itoa(len(h.routes)))

		for _, rt := range h.routes {
			if rt.id == url {
				rt.handle(w, r)
			}
		}
	}
}

// Add is ...
func (h *WebHandler) Add(id string, handle FnRoute) {
	var p = new(WebRoute)

	p.id = id
	p.handle = handle

	h.routes = append(h.routes, p)
}

func getValue(r *http.Request, key string) string {
	keys, ok := r.URL.Query()[key]

	if !ok || len(keys[0]) < 1 {
		log.Println("Url query key " + key + " is missing.")

		return ""
	}

	log.Println("getValue: " + key + "=" + keys[0])

	return keys[0]
}

func handleStatic(w http.ResponseWriter, r *http.Request) {
	data := readFile(r.URL.Path[1:])

	io.WriteString(w, data)
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	log.Println("run handler hello " + r.URL.Path)

	t := readFile("static/index.html")

	io.WriteString(w, t)
}

func handleAuth(w http.ResponseWriter, r *http.Request) {
	log.Println("run handler Auth")

	userName := getValue(r, "user")
	userPass := getValue(r, "pass")

	log.Printf("Current user[%v], pass[%v]", uname, upass)
	log.Printf("Income  user[%v], pass[%v]", userName, userPass)

	if userName != uname || userPass != upass {
		log.Println("Error: wrong credentials. income is " + userName + " " + userPass)

		io.WriteString(w, "NO")

		return
	}

	dtime := time.Since(startDate)

	res := false

	if sessionID == "" {
		uuid := uuid.New().String()
		clientIP = r.RemoteAddr
		sessionID = uuid
		expire := time.Now().Add(10 * time.Minute)
		cookie := http.Cookie{Name: "SID", Value: uuid, Path: "/", Expires: expire, MaxAge: 90000}
		http.SetCookie(w, &cookie)
		startDate = time.Now()

		res = true
	} else if dtime.Minutes() > 10 {
		clientIP = r.RemoteAddr
		expire := time.Now().Add(10 * time.Minute)
		cookie := http.Cookie{Name: "SID", Value: sessionID, Path: "/", Expires: expire, MaxAge: 90000}
		http.SetCookie(w, &cookie)
		startDate = time.Now()

		res = true
	} else if clientIP == r.RemoteAddr {
		expire := time.Now().Add(10 * time.Minute)
		cookie := http.Cookie{Name: "SID", Value: sessionID, Path: "/", Expires: expire, MaxAge: 90000}
		http.SetCookie(w, &cookie)
		startDate = time.Now()

		res = true
	}

	data := ""

	if res {
		data = "YES"
	} else {
		data = "NO"
	}

	io.WriteString(w, data)

	log.Println("Auth result is " + data)
}

func handleMouseMove(w http.ResponseWriter, r *http.Request) {
	log.Println("handle: handleMouseMove. ")

	if !sessionValid(r) {
		return
	}

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")
	var ff = getValue(r, "f")

	//var touchForce float32 = 0.0

	//touchForce, _ := strconv.ParseFloat(ff, 32)

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	//if touchForce > 0.5 {
	//	MouseLeftDown()
	//}

	SetMousePosition(mx, my)

	//if touchForce > 0.5 {
	//	MouseLeftUp()
	//}

	//touchForce = 0.0

	log.Println("handleMouseMove: " + x + " " + y + " " + ww + " " + hh + " " + ff)
}

func handleMouseClick(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)
	ClickMouse(mx, my)
	log.Println("handleMouseClick: " + x + " " + y + " " + ww + " " + hh)
}

func handleMouseDown(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)
	MouseLeftDown()

	//log.Println("handleMouseLeftDown: " + x + " " + y + " " + ww + " " + hh)
}

func handleMouseUp(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)
	MouseLeftUp()

	//log.Println("handleMouseLeftUp: " + x + " " + y + " " + ww + " " + hh)
}

/*
func handleTouchStart(w http.ResponseWriter, r *http.Request) {
	log.Println("handleTouchStart: ")

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)

	MouseLeftDown()
}

func handleTouchEnd(w http.ResponseWriter, r *http.Request) {
	log.Println("handleTouchEnd: ")

	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)

	MouseLeftUp()
}

func handleTouchMove(w http.ResponseWriter, r *http.Request) {
	var x = getValue(r, "x")
	var y = getValue(r, "y")
	var ww = getValue(r, "w")
	var hh = getValue(r, "h")
	var ff = getValue(r, "f")

	fx, _ := strconv.ParseFloat(x, 32)
	fy, _ := strconv.ParseFloat(y, 32)
	fw, _ := strconv.ParseFloat(ww, 32)
	fh, _ := strconv.ParseFloat(hh, 32)
	//fs, _ := strconv.ParseFloat(ff, 32)

	sx := float32(fx / fw)
	sy := float32(fy / fh)

	dw, dh := GetResolution()

	mx := int32(sx * float32(dw))
	my := int32(sy * float32(dh))

	SetMousePosition(mx, my)

	log.Println("handleTouchMove: " + x + " " + y + " " + ww + " " + hh + " " + ff)
}
*/

func handleScreenshot(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	log.Println("handle get_screenshot")

	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("GetScreenshot"))

	if fn == nil {
	   log.Println("GetScreenshot is invalid.")
	   
	   return
	}

	log.Println("starting get_screenshot...")

	scr := C.get_screenshot(fn)

	log.Println("have data from get_screenshot...")

	data := C.GoString(scr)

	io.WriteString(w, data)
	defer C.free(unsafe.Pointer(scr))
	C.dlclose(handle)

	log.Println("Screenshot: ", data)
}

func handleSetAVolume(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	var v = getValue(r, "v")

	iv, err := strconv.ParseInt(v, 10, 32)

	if err == nil && ((iv >= 0) && (iv <= 100)) {
		cmd := exec.Command("pactl", "set-sink-volume", "0", v+"%")
		output, _ := cmd.CombinedOutput()
		fmt.Println(string(output))
	}
}

func handleGetAVolume(w http.ResponseWriter, r *http.Request) {
	if !sessionValid(r) {
		return
	}

	var data string
	var iv int32 = 0

	//cmd := exec.Command("awk -F'[][]' '/Left:/ { print $2 }' <(amixer sget Master)")
	cmd := exec.Command("pactl", "list", "sinks")
	//output, _ := cmd.CombinedOutput()
	//cmd.Run()
	output, err := cmd.Output()

	if err != nil {
		data = "0"
		fmt.Println("Output error: ", err)
	} else {
		lines := strings.Split(strings.ReplaceAll(string(output), "\r\n", "\n"), "\n")

		for _, l := range lines {
			//fmt.Println("Check line: ", l)

			if !strings.Contains(l, "Volume: front-left:") {
				continue
			}

			r := regexp.MustCompile("[0-9]+")

			match := r.FindString(l)

			if match == "" {
				continue
			}

			iv, _ := strconv.ParseInt(match, 10, 32)

			if iv < 0 || iv > 65536 {
				continue
			}

			iv = (iv * 100) / (65536)

			data = strconv.Itoa(int(iv))
		}
	}

	if iv >= 0 && iv <= 100 {
		io.WriteString(w, data)
		fmt.Println("handle GetAVolume: ", data)
	} else {
		fmt.Println("handle GetAVolume: ", data)
		io.WriteString(w, "0")
	}
}

func GetResolution() (int, int) {
	var w C.int = 0
	var h C.int = 0

	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("ScreenResolution"))

  if fn == nil {
    log.Println("ScreenResolution function is NULL")

    return 0, 0
  }

	log.Println("ScreenResolution")

	C.screen_resolution(fn, &w, &h)

	C.dlclose(handle)

	var ww int = -1
	var hh int = -1

	ww = int(w)
	hh = int(h)

	return ww, hh
}

func SetMousePosition(x int32, y int32) {
	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("SetMousePosition"))

	log.Println("SetMousePosition")

	C.set_mouse_position(fn, C.int(x), C.int(y))

	C.dlclose(handle)
}

func GetMousePosition(x *int32, y *int32) {
	*x = 100
	*y = 500
}

func MouseLeftDown() {
	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("MouseLeftDown"))

	log.Println("MouseLeftDown")

	C.mouse_left_down(fn)

	C.dlclose(handle)
}

func MouseLeftUp() {
	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("MouseLeftUp"))

	log.Println("MouseLeftUp")

	C.mouse_left_up(fn)

	C.dlclose(handle)
}

func ClickMouse(x int32, y int32) {
	handle := C.dlopen(C.CString("./xcmd.so"), C.RTLD_LAZY)
	fn := C.dlsym(handle, C.CString("MouseClickLeft"))

	log.Println("ClickMouse: % %", x, y)

	C.mouse_click_left(fn, C.int(x), C.int(y))

	C.dlclose(handle)
	/*
		ButtonPress := 4
		var button C.int = (1 << 8)

		var event C.XEvent

		dpy := C.XOpenDisplay(nil)
		scr := C.XDefaultScreen(dpy)
		rwn := C.XRootWindow(dpy, scr)

		event.xtype = 4
		event.xbutton.button = button
		event.xbutton.same_screen = C.int(1)
		event.xbutton.root = rwn
		event.xbutton.window = 75497473
		event.xbutton.subwindow = 0
		event.xbutton.x_root = x
		event.xbutton.y_root = y
		event.xbutton.x = x
		event.xbutton.y = y
		event.xbutton.state = 0

		C.XSendEvent(dpy, rwn, C.True, C.ButtonPressMask, &event)
		C.XFlush(dsy)
	*/
}

func startServer() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}

	log.Println("Using port: " + port)

	log.Println("Generate new handler.")
	var h = new(WebHandler)

	log.Println("Add handlers.")
	h.Add("/", handleIndex)
	h.Add("/auth", handleAuth)
	h.Add("/mousemove", handleMouseMove)
	h.Add("/mouseclick", handleMouseClick)
	h.Add("/mousedown", handleMouseDown)
	h.Add("/mouseup", handleMouseUp)
	//h.Add("/touchmove", handleTouchMove)
	//h.Add("/touchstart", handleTouchStart)
	//h.Add("/touchend", handleTouchEnd)
	h.Add("/screenshot", handleScreenshot)
	h.Add("/setavolume", handleSetAVolume)
	h.Add("/getavolume", handleGetAVolume)

	log.Println("Set handler.")
	http.Handle("/", h)
	
	log.Println("Start listener.")
	
	err := http.ListenAndServe(":"+port, nil)

	if err != nil {
	   log.Printf("error starting server: %s\n", err)
	}
}

func main() {
	//reader := bufio.NewReader(os.Stdin)
	//fmt.Print("Enter username: ")
	//uname, _ = reader.ReadString('\n')
	//uname = uname[:len(uname)-1]
	//fmt.Print("Enter password: ")
	//pass, _ := reader.ReadString('\n')
	//bpass, err := term.ReadPassword(int(syscall.Stdin))

	//if err != nil {
	//	fmt.Print("ERROR: not valid password")
	//	return
	//}

	//upass = string(bpass)
	//pass = pass[:len(pass)-1]
	//upass = strings.TrimSpace(upass)

	uname = "u"
	upass = "u"
	fmt.Print("registered user/pass: ", uname, "/", upass, "\n")

	startServer()
	/*
		var dpy = C.XOpenDisplay(nil)

		if dpy == nil {
			panic("Can't open display")
		}

		var x int32 = 0
		var y int32 = 0

		GetMousePosition(&x, &y)
		fmt.Println("Mouse position: %i %i", x, y)

		fmt.Println("%ix%i", C.XDisplayWidth(dpy, 0), C.XDisplayHeight(dpy, 0))

		var wnd = C.XRootWindow(dpy, 0)

		var root_x C.int
		var root_y C.int
		var win_x C.int
		var win_y C.int
		var mask C.uint

		var rwnd C.Window
		result := C.XQueryPointer(dpy, wnd, &rwnd, &rwnd, &root_x, &root_y, &win_x, &win_y, &mask)

		if result != 0 {
			fmt.Println("%ix%i", win_x, win_y)
		}

		SetMousePosition(1500, 500)
	*/
}
