#include <ApplicationServices/ApplicationServices.h>
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

extern "C" {
  void ScreenResolution(int* w, int* h);
  void MouseClickLeft();
  void MouseLeftDown();
  void MouseLeftUp();
  void SetMousePosition(int x, int y);
}

static int ms_x = 0, ms_y = 0;

void ScreenResolution(int* w, int* h) {
  NSRect e = [[NSScreen mainScreen] frame];
  *w = (int)e.size.height;
  *h = (int)e.size.width;
}

void MouseClickLeft() {
  MouseLeftUp();
}

void MouseLeftDown() {
  CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
  CGEventRef mouse = CGEventCreateMouseEvent (NULL, kCGEventLeftMouseDown, CGPointMake( ms_x, ms_y), 0);
  CGEventPost(kCGHIDEventTap, mouse);
  CFRelease(mouse);
  CFRelease(source);
}

void MouseLeftUp() {
  CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
  CGEventRef mouse = CGEventCreateMouseEvent (NULL, kCGEventLeftMouseUp, CGPointMake( ms_x, ms_y), 0);
  CGEventPost(kCGHIDEventTap, mouse);
  CFRelease(mouse);
  CFRelease(source);
}

void SetMousePosition(int x, int y) {
  ms_x = x;
  ms_y = y;
  
  CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
  CGEventRef mouse = CGEventCreateMouseEvent (NULL, kCGEventMouseMoved, CGPointMake( x, y), 0);
  CGEventPost(kCGHIDEventTap, mouse);
  CFRelease(mouse);
  CFRelease(source);
}

